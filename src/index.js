import './main.css';
import { Elm } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';

const app = Elm.Main.init({
  node: document.getElementById('root')
});

app.ports.storageCustomer.subscribe(function(data) {
  localStorage.setItem('@crypto:customer', JSON.stringify(data));
});

app.ports.storageTransactions.subscribe(function(data) {
  localStorage.setItem('@crypto:transactions', JSON.stringify(data));
});

registerServiceWorker();
