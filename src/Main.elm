port module Main exposing (Customer, Model, Msg(..), executeOperationCoin, form, initialState, main, subscriptions, sumMoneyTransactions, update, updateCustomer, view)

import Browser
import Coins
import Dict
import GetQuotations as Quotations
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import List
import Task
import Transactions
import Json.Encode as E

port storageCustomer : Customer -> Cmd msg
port storageTransactions : (List Transactions.Transaction) -> Cmd msg

type alias Customer =
    { name : String
    , email : String
    , wallet : Float
    , stored : Bool
    }


type alias Model =
    { loading : Bool
    , customer : Customer
    , quotations : Quotations.Model
    , coinActions : Coins.Model
    , transactions : Transactions.Model
    }


initialState : () -> ( Model, Cmd Msg )
initialState _ =
    ( { loading = False
      , customer = { name = "", email = "", wallet = 0.0, stored = False }
      , quotations =
            { brita =
                { value = []
                }
            , bitcoin =
                { data =
                    { sell = 0.0
                    , buy = 0.0
                    , date = ""
                    }
                }
            }
      , coinActions =
            { value = 0.0
            , operation = ""
            , selected = ""
            }
      , transactions = []
      }
    , Cmd.batch
        [ Cmd.map QuotationsMsg Quotations.getBritaQuotation
        , Cmd.map QuotationsMsg Quotations.getBitcoinQuotation
        ]
    )


type Msg
    = NoOp
    | UpdateName String
    | UpdateEmail String
    | SaveCustomer
    | LoadingQuotations
    | LoadedQuotations
    | QuotationsMsg Quotations.Msg
    | CoinsMsg Coins.Msg
    | TransactionsMsg Transactions.Msg


updateCustomer : (Customer -> Customer) -> Model -> Model
updateCustomer fn model =
    { model | customer = fn model.customer }


executeOperationCoin : Model -> String -> ( Model, Cmd Msg )
executeOperationCoin model operation =
    let
        coin = model.coinActions.selected
        other = 
            if coin == "Brita" then 
                "Bitcoin" 
            else 
                "Brita"

        otherCoin = 
            if operation == "Change" then
                Tuple.first(update (TransactionsMsg (Transactions.CreateTransaction other "Buy")) model )
            else 
                model
        
        operationTransaction = 
            if operation == "Change" then
                "Sell"
            else 
                operation

        ( transactionsUpdated, cmd ) =
            update (TransactionsMsg (Transactions.CreateTransaction model.coinActions.selected operationTransaction)) otherCoin
            
        oldValue =
            model.coinActions

        newCoinAction =
            { oldValue | value = 0 }
    in
    ( 
        { transactionsUpdated | coinActions = newCoinAction }
        , Cmd.batch[
            (storageTransactions transactionsUpdated.transactions),
            (storageCustomer transactionsUpdated.customer)
        ]
    )


sumMoneyTransactions : Transactions.Model -> String -> Float
sumMoneyTransactions transactions coin =
    let
        transactionsItem =
            transactions
                |> List.partition (\x -> x.coin == coin)
                |> Tuple.first
                |> List.partition (\x -> x.operation == "Buy")
                |> Tuple.mapBoth (List.map (\x -> x.money)) (List.map (\x -> negate x.money))

        transactionsValue =
            Tuple.first transactionsItem ++ Tuple.second transactionsItem
    in
    List.sum transactionsValue


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        LoadingQuotations ->
            ( { model | loading = True }, Cmd.none )

        LoadedQuotations ->
            ( { model | loading = False }, Cmd.none )

        UpdateName name ->
            ( updateCustomer (\customer -> { customer | name = name }) model, Cmd.none )

        UpdateEmail email ->
            ( updateCustomer (\customer -> { customer | email = email }) model, Cmd.none )

        SaveCustomer ->
          let
              updatedModel = updateCustomer (\customer -> { customer | wallet = 100000.0, stored = True }) model
          in
          
            (updatedModel , (storageCustomer updatedModel.customer) )

        QuotationsMsg modelMsg ->
            case modelMsg of
                Quotations.LoadQuotation ->
                    ( model, Cmd.none )

                Quotations.GotBrita brita ->
                    case brita of
                        Ok data ->
                            let
                                oldQuotations =
                                    model.quotations

                                newQuotations =
                                    { oldQuotations | brita = data }
                            in
                            ( { model | quotations = newQuotations }, Cmd.none )

                        Err error ->
                            let
                                _ =
                                    Debug.log "Error" error
                            in
                            ( model, Cmd.none )

                Quotations.GotBitcoin bitcoin ->
                    case bitcoin of
                        Ok data ->
                            let
                                oldQuotations =
                                    model.quotations

                                newQuotations =
                                    { oldQuotations | bitcoin = data }
                            in
                            ( { model | quotations = newQuotations }, Cmd.none )

                        Err error ->
                            let
                                _ =
                                    Debug.log "Error" error
                            in
                            ( model, Cmd.none )

        CoinsMsg modelMsg ->
            case modelMsg of
                Coins.SelectCoin coin ->
                    let
                        oldCoins =
                            model.coinActions

                        newCoins =
                            { oldCoins | selected = coin, value = 0 }
                    in
                    ( { model | coinActions = newCoins }, Cmd.none )

                Coins.SetOperation operation ->
                    let
                        oldCoins =
                            model.coinActions

                        newCoins =
                            { oldCoins | operation = operation }
                    in
                    ( { model | coinActions = newCoins }, Cmd.none )

                Coins.UpdateValue quantity ->
                    let
                        oldCoins =
                            model.coinActions

                        newCoins =
                            { oldCoins | value = Maybe.withDefault 0 (String.toFloat quantity) }
                    in
                    ( { model | coinActions = newCoins }, Cmd.none )

                Coins.BuyCoin ->
                    executeOperationCoin model "Buy"

                Coins.SellCoin ->
                    executeOperationCoin model "Sell"

                Coins.ChangeCoin ->
                    executeOperationCoin model "Change"

        TransactionsMsg modelMsg ->
            case modelMsg of
                Transactions.CreateTransaction coin operation ->
                    let

                        value =
                            model.coinActions.value

                        quantityChange = 
                            Coins.calculateQuantityConversionCoin model.coinActions model.quotations

                        calculatedMoney = 
                            Coins.calculateTotalCoin model.coinActions model.quotations

                        moneyWallet = 
                            if operation == "Buy" then
                                model.customer.wallet - calculatedMoney
                            else
                                model.customer.wallet + calculatedMoney
                        
                        customerUpdated = updateCustomer (\customer -> { customer | wallet = moneyWallet }) model

                        transaction =
                            case List.head model.transactions of
                                Just result ->
                                    let
                                        quantity = 
                                            if operation == "Buy" then
                                                if model.coinActions.operation == "Change" then
                                                    quantityChange
                                                else 
                                                    value
                                            else 
                                                negate value                                        
                                    in
                                    { coin = coin
                                    , quantity = quantity
                                    , money = calculatedMoney
                                    , operation = operation
                                    }

                                Nothing ->
                                    { coin = model.coinActions.selected
                                    , quantity = value
                                    , money = calculatedMoney
                                    , operation = operation
                                    }

                        oldTransactions =
                            [ transaction ] ++ model.transactions
                    in
                    ( { model | transactions = oldTransactions, customer = customerUpdated.customer }, Cmd.none)


form : Model -> Html Msg
form model =
    div [ class "row" ]
        [ div [ class "col-md-4 form-group" ]
            [ label [] [ text "nome do cliente" ]
            , input [ class "form-control", onInput UpdateName ] []
            ]
        , div [ class "col-md-4 form-group" ]
            [ label [] [ text "email do cliente" ]
            , input [ class "form-control", onInput UpdateEmail ] []
            ]
        , div [ class "col-md-4 form-group" ]
            [ button
                [ class "button-spacing btn btn-primary"
                , onClick SaveCustomer
                , disabled
                    ((model.customer.name == "") || (model.customer.email == ""))
                ]
                [ text "Criar Conta" ]
            ]
        ]


view : Model -> Html Msg
view model =
    div [] [
        nav [ class "navbar navbar-light bg-light mb-4" ]
            [ a [ class "navbar-brand", href "#" ]
                [ text "Crypto Selling" ],
                if model.customer.stored == True then
                    let
                        name =  "Nome: " ++ model.customer.name
                        email = "Email: " ++ model.customer.email
                        money = "R$ " ++ (String.fromFloat model.customer.wallet)

                        brita = "Britas: " ++ (String.fromFloat (Coins.sumQuantityTransactions model.transactions "Brita"))
                        bitcoins = "Bitcoins: " ++ (String.fromFloat (Coins.sumQuantityTransactions model.transactions "Bitcoin"))
                    in
                        div [ class "navbar-nav navbar-expand-lg" ]
                            [ a [ class "nav-item nav-link mr-4", href "#" ]
                                [ text name ]
                            , a [ class "nav-item nav-link mr-4", href "#" ]
                                [ text email ]
                            , a [ class "nav-item nav-link mr-4", href "#" ]
                                [ text bitcoins ]
                            , a [ class "nav-item nav-link mr-4", href "#" ]
                                [ text brita ]
                            , a [ class "nav-item nav-link mr-4", href "#" ]
                                [ div [class "badge badge-primary"] [
                                    text money
                                ] ]
                        ]
                else
                    text ""
            ]
        , div [ class "container" ]
            [ 
             if model.customer.stored == False then
                form model
            else 
                text ""
            , if model.customer.stored == True then
                Html.map CoinsMsg (Coins.view model.coinActions model.quotations model.transactions)
            else
                text ""
            , Html.map TransactionsMsg (Transactions.view model.transactions)
            ]
    ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


main =
    Browser.element
        { init = initialState
        , subscriptions = subscriptions
        , update = update
        , view = view
        }
