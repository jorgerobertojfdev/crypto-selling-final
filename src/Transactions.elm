module Transactions exposing (Model, Msg(..), Transaction, view)

import Html exposing (..)
import Html.Attributes exposing (..)
import List exposing (..)


type alias Transaction =
    { coin : String
    , quantity : Float
    , money : Float
    , operation : String
    }


type alias Model =
    List Transaction


type Msg
    = CreateTransaction String String

showTransactions : Model -> Html Msg
showTransactions transactions =
    div [] [
        h1 [] [text "Extrato"]
        , if List.isEmpty transactions then
            p [] [
                text "Nenhuma transação"
            ]
        else
            ul [class "m-0 p-0"] (List.map transactionItem transactions)
    ]

transactionItem : Transaction -> Html Msg
transactionItem transaction =
    let 
        iconClass =
            if transaction.coin == "Brita" then 
                "fas fa-coins mr-2"
            else
                "fab fa-bitcoin mr-2"
        operation =
            if transaction.operation == "Buy" then
                "Compra"
            else
                "Venda"
        badgeClass =
            if transaction.operation == "Buy" then
                "pull-right badge badge-primary"
            else
                "pull-right badge badge-danger"

    in
        li [class "card mb-4"]
            [
                div [class "card-header"] [
                    div [class "pull-left"] [
                        i [ class iconClass ] []
                        , text transaction.coin
                    ],
                    div [class badgeClass] [
                        text operation
                    ]
                ],
                div [class "card-body"] [
                    p [] [
                        text "Quantidade: "
                        , b [] [
                            text (String.fromFloat transaction.quantity)
                        ]
                    ],
                    p [] [
                        text "Preço: "
                        , b [] [
                            text (String.fromFloat transaction.money)
                        ]
                    ]
                ]
            ]

view : Model -> Html Msg
view model =
    showTransactions model
