module GetQuotations exposing (Bitcoin, BitcoinFields, Brita, BritaFields, Model, Msg(..), bitcoinDecoder, bitcoinEndpoint, bitcoinFieldsDecoder, britaDecoder, britaEndpoint, britaFieldsDecoder, getBitcoinQuotation, getBritaQuotation)

import Http
import Json.Decode exposing (Decoder, field, float, int, list, map, map3, maybe, string)


britaEndpoint : String
britaEndpoint =
    "https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarDia(dataCotacao=@dataCotacao)?@dataCotacao='05-21-2019'&$top=100&$format=json&$select=cotacaoCompra,cotacaoVenda,dataHoraCotacao"


bitcoinEndpoint : String
bitcoinEndpoint =
    "https://api.bitcointrade.com.br/v2/public/BRLBTC/ticker"


type alias BritaFields =
    { cotacaoCompra : Float
    , cotacaoVenda : Float
    , dataHoraCotacao : String
    }


type alias Brita =
    { value : List BritaFields
    }


type alias BitcoinFields =
    { buy : Float
    , sell : Float
    , date : String
    }


type alias Bitcoin =
    { data : BitcoinFields
    }


type alias Model =
    { brita : Brita
    , bitcoin : Bitcoin
    }


type Msg
    = LoadQuotation
    | GotBrita (Result Http.Error Brita)
    | GotBitcoin (Result Http.Error Bitcoin)


britaFieldsDecoder : Decoder BritaFields
britaFieldsDecoder =
    map3 BritaFields
        (field "cotacaoCompra" float)
        (field "cotacaoVenda" float)
        (field "dataHoraCotacao" string)


britaDecoder : Decoder Brita
britaDecoder =
    map Brita
        (field "value" (list britaFieldsDecoder))


bitcoinFieldsDecoder : Decoder BitcoinFields
bitcoinFieldsDecoder =
    map3 BitcoinFields
        (field "buy" float)
        (field "sell" float)
        (field "date" string)


bitcoinDecoder : Decoder Bitcoin
bitcoinDecoder =
    map Bitcoin
        (field "data" bitcoinFieldsDecoder)


getBritaQuotation : Cmd Msg
getBritaQuotation =
    Http.get
        { url = britaEndpoint
        , expect = Http.expectJson GotBrita britaDecoder
        }


getBitcoinQuotation : Cmd Msg
getBitcoinQuotation =
    Http.get
        { url = bitcoinEndpoint
        , expect = Http.expectJson GotBitcoin bitcoinDecoder
        }
