module Coins exposing (Model, Msg(..), actionsComponent, calculateQuantityConversionCoin, calculateTotalCoin, coinComponent, disabledSell, getBritaRecord, getTaxBitcoin, getTaxBrita, numberInput, quotationsInfo, quotationsInfoItem, sumQuantityTransactions, view)

import GetQuotations as Quotations
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Transactions


type alias Model =
    { value : Float
    , operation : String
    , selected : String
    }


type Msg
    = SelectCoin String
    | SetOperation String
    | UpdateValue String
    | BuyCoin
    | SellCoin
    | ChangeCoin


getBritaRecord : Quotations.Brita -> Quotations.BritaFields
getBritaRecord quotation =
    case List.head quotation.value of
        Just value ->
            value

        Nothing ->
            Quotations.BritaFields 0.0 0.0 "0"


getTaxBrita : String -> Quotations.Brita -> Float -> Float
getTaxBrita operation quotation quantity =
    let
        tax =
            getBritaRecord quotation
    in
    if operation == "Sell" || operation == "Change" then
        tax.cotacaoVenda * quantity

    else if operation == "Buy" then
        tax.cotacaoCompra * quantity

    else
        0.0


getTaxBitcoin : String -> Quotations.Bitcoin -> Float -> Float
getTaxBitcoin operation quotation quantity =
    let
        tax =
            quotation.data
    in
    if operation == "Sell" || operation == "Change" then
        tax.sell * quantity

    else if operation == "Buy" then
        tax.buy * quantity

    else
        0.0


calculateQuantityConversionCoin : Model -> Quotations.Model -> Float
calculateQuantityConversionCoin model quotations =
    let
        value =
            model.value

        tax =
            case model.selected of
                "Brita" ->
                    convertBritaToBtc model.value quotations

                "Bitcoin" ->
                    convertBtcToBrita model.value quotations
                _ ->
                    0.0
    in
        tax

calculateTotalCoin : Model -> Quotations.Model -> Float
calculateTotalCoin model quotations =
    let
        value =
            model.value

        tax =
            case model.selected of
                "Brita" ->
                    getTaxBrita model.operation quotations.brita value

                "Bitcoin" ->
                    getTaxBitcoin model.operation quotations.bitcoin value

                _ ->
                    0.0
    in
        tax


sumQuantityTransactions : Transactions.Model -> String -> Float
sumQuantityTransactions transactions coin =
    let
        transactionsItem =
            transactions
                |> List.partition (\x -> x.coin == coin)
                |> Tuple.first
                |> List.partition (\x -> x.operation == "Buy")
                |> Tuple.mapBoth (List.map (\x -> x.quantity)) (List.map (\x -> x.quantity))

        transactionsValue =
            Tuple.first transactionsItem ++ Tuple.second transactionsItem

    in
    List.sum transactionsValue


disabledSell : Model -> Transactions.Model -> Bool
disabledSell model transactions =
    let
        quantities =
            sumQuantityTransactions transactions model.selected
    in
    quantities == 0 || (model.value > quantities || model.value == 0)


disabledExchange : Model -> Quotations.Model -> Transactions.Model -> Bool
disabledExchange model quotations transactions =
    let
        quantities =
            sumQuantityTransactions transactions model.selected

        _ =
            Debug.log "quantities" quantities

        _ =
            Debug.log "awda" (calculateTotalCoin model quotations)
    in
    quantities == 0 || model.value > quantities


numberInput : (String -> Msg) -> Float -> Html Msg
numberInput msg inputValue =
    input [ 
        type_ "number", 
        placeholder "Total", 
        onInput msg, 
        value (String.fromFloat inputValue),
        class "form-control"
    ] []


convertBritaToBtc : Float -> Quotations.Model -> Float
convertBritaToBtc money quotations =
    let
        priceBrita =
            getTaxBrita "Change" quotations.brita money

        priceBtc =
            priceBrita / quotations.bitcoin.data.buy
    in
    priceBtc


convertBtcToBrita : Float -> Quotations.Model -> Float
convertBtcToBrita money quotations =
    let
        brita =
            getBritaRecord quotations.brita

        priceBtc =
            getTaxBitcoin "Change" quotations.bitcoin money

        priceBrita =
            priceBtc / brita.cotacaoCompra
    in
    priceBrita


quotationsInfoItem : Float -> Float -> Html Msg
quotationsInfoItem sell buy =
    div []
        [ div [ class "badge badge-success" ]
            [ text ("Preço de venda: R$ " ++ String.fromFloat sell)
            ]
        , div [ class "ml-2 badge badge-primary" ]
            [ text ("Preço de Compra: R$ " ++ String.fromFloat buy)
            ]
        ]


quotationsInfo : Model -> Quotations.Model -> Html Msg
quotationsInfo model quotations =
    if model.selected == "Brita" then
        let
            quotation =
                getBritaRecord quotations.brita
        in
        quotationsInfoItem quotation.cotacaoVenda quotation.cotacaoCompra

    else
        quotationsInfoItem quotations.bitcoin.data.sell quotations.bitcoin.data.buy


actionsComponent : Model -> Quotations.Model -> Html Msg
actionsComponent model quotations =
    div [class "mt-4"]
        [ div [ class "form-check form-check-inline" ]
            [ input
                [ class "form-check-input"
                , type_ "radio"
                , id "buy_coin"
                , name "operation_coin"
                , onClick (SetOperation "Buy")
                ]
                []
            , label [ class "form-check-label", for "buy_coin" ] [ text "Comprar" ]
            ]
        , div [ class "form-check form-check-inline" ]
            [ input
                [ class "form-check-input"
                , type_ "radio"
                , id "sell_coin"
                , name "operation_coin"
                , onClick (SetOperation "Sell")
                ]
                []
            , label [ class "form-check-label", for "sell_coin" ] [ text "Vender" ]
            ]
        , div [ class "form-check form-check-inline" ]
            [ input
                [ class "form-check-input"
                , type_ "radio"
                , id "change_coin"
                , name "operation_coin"
                , onClick (SetOperation "Change")
                ]
                []
            , label [ class "form-check-label", for "change_coin" ] [ text "Trocar" ]
            ]
        , text ("$ " ++ String.fromFloat (calculateTotalCoin model quotations))
        ]


coinComponent : Html Msg
coinComponent =
    div []
        [ div [ class "form-check form-check-inline" ]
            [ input
                [ class "form-check-input"
                , type_ "radio"
                , id "brita_coin"
                , name "coin"
                , value "Brita"
                , onClick (SelectCoin "Brita")
                ]
                []
            , label [ class "form-check-label", for "brita_coin" ]
                [ text "Brita" ]
            ]
        , div [ class "form-check form-check-inline" ]
            [ input
                [ class "form-check-input"
                , type_ "radio"
                , id "bitcoin_coin"
                , value "Bitcoin"
                , name "coin"
                , onClick (SelectCoin "Bitcoin")
                ]
                []
            , label [ class "form-check-label", for "bitcoin_coin" ] [ text "Bitcoin" ]
            ]
        ]


view : Model -> Quotations.Model -> Transactions.Model -> Html Msg
view model quotations transactions =
    div [class "my-4"]
        [ coinComponent
        , if model.selected /= "" then
            div []
                [ quotationsInfo model quotations
                , actionsComponent model quotations
                ]

          else
            text ""
        , if model.operation /= "" then
            div [class "input-group my-4"] 
                [
                    numberInput UpdateValue model.value
                    , div [class "input-group-append"] [
                        if model.operation == "Buy" then
                            button [ class "btn btn-outline-secondary", onClick BuyCoin ] [ text "Comprar" ]

                        else if model.operation == "Sell" then
                            button [ class "btn btn-outline-secondary", onClick SellCoin, disabled (disabledSell model transactions) ] [ text "Vender" ]

                        else if model.operation == "Change" then
                            button [ class "btn btn-outline-secondary", onClick ChangeCoin, disabled (disabledExchange model quotations transactions) ] [ text "Trocar" ]

                        else
                            text ""
                    ]

                ]
          else
            text ""
        ]
